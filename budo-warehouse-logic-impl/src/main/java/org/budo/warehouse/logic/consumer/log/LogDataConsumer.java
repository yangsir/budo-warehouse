package org.budo.warehouse.logic.consumer.log;

import org.budo.warehouse.logic.api.AbstractDataConsumer;
import org.budo.warehouse.logic.api.DataMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j(topic = "warehouse.consumer.log")
public class LogDataConsumer extends AbstractDataConsumer {
    @Override
    public void consume(DataMessage dataMessage) {
        log.info("#20 dataMessage=" + dataMessage + ", pipeline=" + this.getPipeline());
    }
}