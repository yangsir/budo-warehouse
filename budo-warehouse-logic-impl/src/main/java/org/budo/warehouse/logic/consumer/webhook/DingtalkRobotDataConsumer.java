package org.budo.warehouse.logic.consumer.webhook;

import org.budo.graph.annotation.SpringGraph;
import org.budo.warehouse.logic.api.AbstractDataConsumer;
import org.budo.warehouse.logic.api.DataMessage;

/**
 * https://open-doc.dingtalk.com/microapp/serverapi2/qf2nxq
 * 
 * @author lmw
 */
public class DingtalkRobotDataConsumer extends AbstractDataConsumer {
    @SpringGraph
    @Override
    public void consume(DataMessage dataMessage) {
        super.consume(dataMessage); // HTTP POST
    }
}