package org.budo.warehouse.logic.api;

import java.io.Serializable;
import java.util.List;

import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lmw
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataMessagePojo implements DataMessage, Serializable {
    private static final long serialVersionUID = -3590590864686418125L;

    private Long id;

    private Integer dataNodeId;

    private List<DataEntry> dataEntries;
}