package org.budo.warehouse.logic.util;

import java.util.List;

import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataEntryPojo.Row;

import com.alibaba.fastjson.JSON;

/**
 * @author limingwei
 */
public class DataEntryUtil {
    public static boolean hasIsKeyColumn(DataEntry dataEntry, Integer rowIndex) {
        Integer columnCount = dataEntry.getColumnCount(rowIndex);

        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            Boolean columnIsKey = dataEntry.getColumnIsKey(rowIndex, columnIndex);
            if (columnIsKey) {
                return true;
            }
        }

        return false;
    }

    public static String rowsToJson(List<Row> rows) {
        return JSON.toJSONString(rows);
    }

    public static List<Row> jsonToRows(String json) {
        return JSON.parseArray(json, Row.class);
    }
}